import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the MoovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MoovieProvider {
  private baseApiPath = "https://api.themoviedb.org/3";
  private key         = "26553a535b1be2c81c0a036403fb4963";
  private api_key     = "?api_key=";
  private movie       = "/movie"
  private latest      = "/latest";
  private popular     = "/popular"
  
  constructor(public http: Http) {
    
  }

  getLatestMovies(){
    return this.http.get(this.baseApiPath + this.movie + this.popular + this.api_key + this.key);
  }

}

//https://api.themoviedb.org/3/movie/550?api_key=26553a535b1be2c81c0a036403fb4963

//
///movie/latest